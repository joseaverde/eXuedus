# eXuedus



## Getting started


## Add your files



## Integrate with your tools


## Collaborate with your team



## Test and Deploy


## Name
eXuedus [/ˈeksədəs/]

## Description
Adaptación del clásico juego Lemmings hecho en Godot con C++. 

## Construir 
**Requisitos**
- CMake v.3.19 como mínimo
- Godot 4
- El repo duh

En el directorio /build/ con -j<número> podeís ajustar la cantidad de núcleos(jobs) que le asignaís, para no estaros todo el día:
```
$ make 
$ make install
```
Éste último copia la librería generada al directorio donde lo busca Godot.

## Hacer un módulo
**Necesitareís**
- Un header en el directorio > src/include/ejemplo.hpp
- El archivo cpp en el directorio > src/src/ejemplo.cpp

Modificar el archivo:
 - src/src/init/gdextension_registration.cpp

Para añadir la clase (podéis ver cómo lo ha hecho JoseA para la clase Mice, es un #include  y una línea)

En el archivo .hpp, añadid la estructura de Mice, heredáis de la clase que prefiráis, y hay que escribir dos métodos importantes _process y _bind_methods:
 - _process: se ejecuta en cada frame, hay va la lógica (si os dais cuenta llamo al método de la clase padre:  ClassDB::register_class<GDExample>();)
 - _bind_methods: se ejecuta al crear la clase, hay añadís los métodos en texto para que Godot sepa qué hay

## Roadmap
Puerta

## Contributing
NA

## Authors and acknowledgment
:O

## License
TBD

## Project status
Null
