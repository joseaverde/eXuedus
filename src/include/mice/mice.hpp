/* *** HEADER *** */

#ifndef __EXUEDUS_MICE_HPP
#define __EXUEDUS_MICE_HPP

#include <cstdint>
#include <godot_cpp/godot.hpp>
#include <godot_cpp/classes/rigid_body3d.hpp>

namespace godot {

  class Mice : public RigidBody3D {
    GDCLASS(Mice, RigidBody3D)

  public:

    static void _bind_methods (void);

    void _process (double_t delta) override;

    void set_speed (float_t speed) {
      m_speed = speed ;
    }

    [[nodiscard]]
    float_t get_speed (void) const {
      return m_speed ;
    }

  private:

    void process_movement (double_t delta);
    float_t m_speed = 4.9f;
    float_t push_force = 10; //10 Newton
  };
}

#endif//__EXUEDUS_MICE_HPP
