#pragma once

#include <istream>
#include <vector>
#include <list>
#include <cstdint>

struct vec3 { double x, y, z; };

namespace eXuedus {

class Terrain {
  public:
    Terrain (std::istream & file);

    // Godot methods
    // static void _bind_methods ();
    void _process (double delta); // override;
    // bool collide (godot::RigidBody3D const& other) const noexcept;
    // void show

    // Terrain methods
    void explode (vec3 const& coords, double strength) noexcept;
    void dig (vec3 const& coords, vec3 const& angles) noexcept;
    void stairs (vec3 const& coords) noexcept;

  private:

    static constexpr double dig_radius = 10.0;
    static constexpr vec3 stair_size{10.0, 10.0, 10.0};

    struct Explosion {
      vec3   centre;
      double strength;
      double radius; };

    struct Dig {
      vec3   start;
      vec3   angle;
      double radius; };

    struct Stairs {
      vec3 position;
      vec3 size; };

    // Returns true if it has finished.
    bool process_explosion (Explosion & explosion, double delta) noexcept;
    bool process_dig (Dig & dig, double delta) noexcept;
    bool process_stairs (Stairs & stairs, double delta) noexcept;

    using Block = uint8_t;

    std::list<Explosion> explosions_;
    std::list<Dig>       holes_;
    std::list<Stairs>    stairs_;
    std::vector<std::vector<std::vector<Block>>> blocks_;
    int32_t width_;
    int32_t height_;
    int32_t depth_;
};

}

// Basher   --→
// Miner     \
// Digger    ↓

// Bomber    O

// Bridger   .-'

// Jumper
// Blocker
// Floater
