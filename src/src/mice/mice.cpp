/* *** CPP *** */

#include <mice/mice.hpp>
#include <godot_cpp/core/class_db.hpp>
#include <godot_cpp/variant/utility_functions.hpp>
#include <godot_cpp/classes/input.hpp>

namespace godot {

  void Mice::_process (double_t delta) {
    RigidBody3D::_process (delta);
    process_movement (delta);
  }

  void Mice::process_movement (double_t delta) {

    Vector3 dir{0.0f, 0.0f, 0.0f};

//    Input& inputSingleton = *Input::get_singleton();

//    if (inputSingleton.is_action_pressed("ui_right"))
//    {
//        UtilityFunctions::print ("ui_right");
//        dir.x += 1.0f;
//    }
//    if (inputSingleton.is_action_pressed("ui_left"))  dir.x -= 1.0f;
//    if (inputSingleton.is_action_pressed("ui_up"))    dir.y += 1.0f;
//    if (inputSingleton.is_action_pressed("ui_down"))  dir.y -= 1.0f;


    //push forward
    dir.x = 1.0f;
//    set_position(get_position() + (dir * m_speed * delta));
    apply_impulse(0.01*dir * push_force);
//    set_rotation(dir*2*M_PI);

    // UtilityFunctions::print(dir.x, dir.y);
//    UtilityFunctions::print (get_position());

//    set_position (get_position() + (dir * m_speed * delta));
//    x = x0 + v*dt
  }

  void Mice::_bind_methods (void) {

    ClassDB::bind_method (D_METHOD("get_speed"), &Mice::get_speed);
    ClassDB::bind_method (D_METHOD("set_speed", "speed"), &Mice::set_speed);

    ADD_GROUP ("MICE", "mice_");
    ADD_PROPERTY (PropertyInfo (Variant::FLOAT, "mice_speed"), "set_speed", "get_speed");
  }

}
