#include "terrain/terrain.hpp"

namespace eXuedus {

/* [MAGIC NUMBER] [WIDTH(X)] [HEIGHT(Y)] [DEPTH(Z)] [HEADER SIZE]
 * ...  [NAME] [MICE] ...
 * [MAP]
 */

// Serialised?
Terrain::Terrain (std::istream & file) {
  char magic[4];
  int32_t header;
  Block b;
  file >> magic >> width_ >> height_ >> depth_ >> header;
  for (auto i = 0; i < header; i++)
    static_cast<void>(file.get());
  blocks_.reserve(width_);
  for (auto x = 0; x < width_; x++) {
    std::vector<std::vector<Block>> plane;
    plane.reserve(height_);
    for (auto y = 0; y < height_; y++) {
      std::vector<Block> line;
      line.reserve(depth_);
      for (auto z = 0; z < depth_; z++) {
        file >> b;
        line.push_back(b);
      }
      plane.push_back(std::move(line));
    }
    blocks_.push_back(std::move(plane));
  }
}

void Terrain::_process (double delta) {
  for (auto it = explosions_.begin(); it != explosions_.end();)
    if (process_explosion (*it, delta))
      it = explosions_.erase (it);
  for (auto it = holes_.begin(); it != holes_.end();)
    if (process_dig (*it, delta))
      it = holes_.erase (it);
  for (auto it = stairs_.begin(); it != stairs_.end();)
    if (process_stairs(*it, delta))
      it = stairs_.erase (it);
}

void Terrain::explode (vec3 const& coords, double strength) noexcept {
  explosions_.emplace_back(coords, strength, 0.0);
}

void Terrain::dig (vec3 const& coords, vec3 const& angles) noexcept {
  holes_.emplace_back(coords, angles, dig_radius);
}

void Terrain::stairs (vec3 const& coords) noexcept {
  stairs_.emplace_back(coords, stair_size);
}

bool Terrain::process_explosion (Explosion & explosion, double delta) noexcept {
  return true;
}

bool Terrain::process_dig (Dig & dig, double delta) noexcept {
  return true;
}

bool Terrain::process_stairs (Stairs & stairs, double delta) noexcept {
  return true;
}

}
