#include <mice/mice.hpp>

#include <godot_cpp/godot.hpp>
#include <godot_cpp/core/class_db.hpp>

using namespace godot;

static void
register_eXuedus_types (ModuleInitializationLevel p_level) {

  if (p_level != ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
    return;

  // Register classes here
  ClassDB::register_class<Mice>();
}

static void
unregister_eXuedus_types (ModuleInitializationLevel p_level) {

  if (p_level != ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
    return ;
  // Do nothing :)
}

extern "C" {

  GDExtensionBool GDE_EXPORT
  eXuedus_library_init (
      const GDExtensionInterface *p_interface,
      GDExtensionClassLibraryPtr p_library,
      GDExtensionInitialization  *r_initialization ) {

    GDExtensionBinding::InitObject init_object (
        p_interface, p_library, r_initialization );

    init_object.register_initializer (register_eXuedus_types);
    init_object.register_terminator (unregister_eXuedus_types);
    init_object.set_minimum_library_initialization_level (
        ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE);

    return init_object.init ();
  }

}
